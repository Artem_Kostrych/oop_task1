package com.kostrych.music;

import com.kostrych.person.Composer;
import com.kostrych.person.Performer;

public class Music {
    private String genreName;
    private Performer performer;
    private double duration;
    private Composer composer;

    public Music(String genreName, Performer performer, double duration, Composer composer) {
        this.genreName = genreName;
        this.performer = performer;
        this.duration = duration;
        this.composer = composer;
    }

    @Override
    public String toString() {
        return "Music{" +
                "genreName='" + genreName + '\'' +
                ", performer=" + performer +
                ", duration=" + duration +
                ", composer=" + composer +
                '}'+"\n";
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Performer getPerformer() {
        return performer;
    }

    public void setPerformer(Performer performer) {
        this.performer = performer;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Composer getComposer() {
        return composer;
    }

    public void setComposer(Composer composer) {
        this.composer = composer;
    }
}
