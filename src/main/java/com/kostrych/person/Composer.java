package com.kostrych.person;

public class Composer extends Person {
    public Composer(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Composer{" +
                "name='" + name + '\'' +
                '}';
    }
}
