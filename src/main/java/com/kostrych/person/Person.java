package com.kostrych.person;

public abstract class Person {

    protected String name;

    public Person(String name) {
        this.name = name;
    }
}
