package com.kostrych.person;

import com.kostrych.CD;
import com.kostrych.music.Music;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Worker extends Person {

    public Worker(String name) {
        super(name);
    }

    public List<Music> findMusicByGenre(List<CD> cdList, CD disk) {
        List<Music> musicList = new ArrayList<>();
        System.out.println("Input your genre: ");
        Scanner in = new Scanner(System.in);
        String genre = in.nextLine();
        Music musicInput;
        for (CD cd : cdList) {
            for (Music music : cd.getMusicList()) {
                if (music.getGenreName().equalsIgnoreCase(genre)) {
                    if (disk.CAPACITY - disk.getBusyMemory() > music.getDuration()) {
                        musicList.add(music);
                        disk.setBusyMemory(disk.getBusyMemory()+music.getDuration());
                    }
                }
            }
        }
        return musicList;
    }
}
