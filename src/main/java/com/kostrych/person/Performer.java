package com.kostrych.person;

public class Performer extends Person {
    public Performer(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Performer{" +
                "name='" + name + '\'' +
                '}';
    }
}
