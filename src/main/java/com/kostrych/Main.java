package com.kostrych;

import com.kostrych.music.Music;
import com.kostrych.person.Composer;
import com.kostrych.person.Performer;
import com.kostrych.person.Worker;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<CD> CDList = new ArrayList<>();
        List<Music> musicList = new ArrayList<>();
        musicList.add(new Music("Classical", new Performer("Artem"),
                3.13, new Composer("Oleg")));
        musicList.add(new Music("Classical", new Performer("Artem"),
                2.13, new Composer("Petro")));
        musicList.add(new Music("Pop", new Performer("Artem"),
                6.13, new Composer("Vasya")));
        musicList.add(new Music("Classical", new Performer("Artem"),
                1.13, new Composer("Ivan")));
        musicList.add(new Music("Classical", new Performer("Artem"),
                6.13, new Composer("Ivan")));

        CDList.add(new CD(musicList));
        List<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan"));
        MusicShop musicShop = new MusicShop(CDList, workers);

        System.out.println(musicShop.getWorkers().get(0).findMusicByGenre(musicShop.getCDList(), new CD()));
    }
}
