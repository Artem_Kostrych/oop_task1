package com.kostrych;

import com.kostrych.music.Music;

import java.util.List;

public class CD {
    public static double CAPACITY = 10;
    private double busyMemory;
    private List<Music> musicList;

    public static double getCAPACITY() {
        return CAPACITY;
    }

    public static void setCAPACITY(double CAPACITY) {
        CD.CAPACITY = CAPACITY;
    }

    public double getBusyMemory() {
        return busyMemory;
    }

    public void setBusyMemory(double busyMemory) {
        this.busyMemory = busyMemory;
    }

    public CD() {
    }

    public CD(List<Music> musicList) {
        this.musicList = musicList;
    }

    public List<Music> getMusicList() {
        return musicList;
    }

    public void setMusicList(List<Music> musicList) {
        this.musicList = musicList;
    }
}
