package com.kostrych;

import com.kostrych.music.Music;
import com.kostrych.person.Worker;

import java.util.List;

public class MusicShop {
    private List<CD> CDList;
    private List<Worker> workers;

    public MusicShop(List<CD> CDList, List<Worker> workers) {
        this.CDList = CDList;
        this.workers = workers;
    }

    public MusicShop() {
    }

    public List<CD> getCDList() {
        return CDList;
    }

    public void setCDList(List<CD> CDList) {
        this.CDList = CDList;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }
}
